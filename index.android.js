import React, {Component} from 'react';
import GridView from 'react-native-grid-view';
import {
  AppRegistry,
  Alert,
  ListView,
  Text,
  View,
  Button,
  TouchableHighlight,
  StyleSheet,
  Image,
  ToastAndroid,
  TouchableOpacity
} from 'react-native';

const Dimensions = require('Dimensions');
const BASE_SIZE = 30;
const RANDOM_FACTOR_MAX = 30;

var MOVIES_PER_ROW = 3;
var videonumber = 1;

var connectionEstablished = false;

//TODO error handling for socket
var socket = new WebSocket('ws://192.168.178.64:8082/');

socket.onopen = () => {
  // connection opened
  console.log("Socket connection opened")
  connectionEstablished = true;
};

socket.onmessage = (e) => {
  // a message was received
  console.log(e.data);
};

socket.onerror = (e) => {
  // an error occurred
  console.log(e.message);
};

socket.onclose = (e) => {
  // connection closed
  console.log(e.code, e.reason);
  connectionEstablished = false;
};

class Video extends Component {

  constructor(props) {
    console.log("calld")
    super(props);
    this._handleOnPress = this._handleOnPress.bind(this)
    this.state = { open: false };
    this.state = { pressStatus: false };
    this.state = { currentVideo: "bull"};
  }

  render() {
    handleOnPress = this._handleOnPress.bind(this);
    return (
      <TouchableHighlight onPress={() => this._handleOnPress()}>
        <View style={styles.video} >
          <Image
            source={{uri: this.props.video.thumbnail}}
            style={styles.thumbnail}
          />
          <View >
            <Text
              style={styles.textSmall}
              numberOfLines={3}>{this.props.video.title}</Text>
              <Text style={styles.year}>{this.props.video.year}</Text>
            </View>

          </View>
        </TouchableHighlight>
      );
    }

    _handleOnPress=() => {

      if(connectionEstablished) {
        this.props.nowPlaying = true;
        Alert.alert(
          "Video playing",
          "Currently playing video '" + this.props.video.title + "'",
          [
              {text: 'Pause video', onPress: () => socket.send("pause video")},
              {text: 'Ok', onPress: () => console.log("ok")}
          ],
          { cancelable: true }
        );
        //change to bull, cowwalking, mudhouse

        socket.send(this.props.video.video + ".mp4");
      } else {
        Alert.alert(
          "No connection to the server",
          "Please check your connection"
        );
      }
    }
  }

  class ProtoApp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            dataSource: null,
            loaded: false
        }
    }

    componentDidMount() {
      this.fetchData();
    }

    fetchData() {
      this.setState({
        //TODO populate datadource properly
        dataSource: [
          {key: 0, title: 'Bull', thumbnail: "bull", video: "bull"},
          {key: 1, title: 'Mudhouse', thumbnail: "mudhouse", video: "mudhouse"},
          {key: 2, title: 'Yellow Cow', thumbnail: "yellowcow", video: "cowwalking"},
          {key: 3, title: 'Hut and Himba woman', thumbnail: "houseandwoman", video: "houseandwoman"},
          {key: 4, title: 'Himba woman', thumbnail: "himbawomanstanding", video: "himbawomanstanding"}
        ],
        //        dataSource: [
        //            {key: 0, title: 'Bull', thumbnail: "bull"},
        //            {key: 1, title: 'Bull', thumbnail: "mudhouse"},
        //            {key: 2, title: 'Bull', thumbnail: "yellowcow"},
        //            {key: 3, title: 'Bull', thumbnail: "bull"},
        //            {key: 4, title: 'Bull', thumbnail: "mudhouse"},
        //            {key: 5, title: 'Bull', thumbnail: "yellowcow"},
        //            {key: 6, title: 'Bull', thumbnail: "bull"},
        //            {key: 7, title: 'Bull', thumbnail: "mudhouse"},
        //            {key: 8, title: 'Bull', thumbnail: "yellowcow"},
        //            {key: 9, title: 'Bull', thumbnail: "bull"}
        //        ],
        loaded: true,
      });
    }

    render() {
      if (!this.state.loaded) {
        return this.renderLoadingView();
      }
      return (
        <View style={styles.view}>
          <Text style={styles.navbar}>
            Holodisplay controller application
          </Text>

          <Text style={styles.text}>
            <Text style={styles.heading}>
              Welcome to the 3D holographic display controller application. {'\n'}
            </Text>
            <Text>
              Please select one of the videos below. {'\n'}{'\n'}
            </Text>
          </Text>

          <GridView
            items={this.state.dataSource}
            itemsPerRow={MOVIES_PER_ROW}
            renderItem={this.renderItem}
            style={styles.listView}
          />
          <NowPlayingFooterView nowPlaying={this.state.nowPlaying}/>
        </View>
      );
    }

    renderLoadingView() {
      return (
        <View>
          <Text style={styles.loadingView}>
            Loading movies...
          </Text>
        </View>
      );
    }

    renderItem(item) {
      return (
        <Video video={item} />
      );
    }
  }


var NowPlayingFooterView = React.createClass({
    componentDidMount: function() {
      //NowPlayingStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function() {
      //NowPlayingStore.removeChangeListener(this.onChange);
    },
    
    setVisible: function () {
        this.setState({vidPlaying: true})  
    },

    onChange: function() {
      this.setState({
        nowPlaying: this.props.nowPlaying
      });
    },

    getInitialState: function () {
      return {
        nowPlaying: this.props.nowPlaying
      }
    },

    getNowPlaying: function () {
      return {
        track: NowPlayingStore.getTrack(),
        playState: NowPlayingStore.getState(),
        playbackTime: NowPlayingStore.getPlaybackTime()
      }
    },

    render: function () {
        if (!this.state.vidPlaying) {
            return (
                <View style={styles.nowPlayingFooter}>
                    <Text style={styles.trackTitle}>Welcome!</Text>
                    <Text style={styles.trackArtist}>Please select a video</Text>
                </View>
            );
        } else {
            return (
                <View style={styles.nowPlayingFooter}>
                    <Text style={styles.trackTitle}>Hallo</Text>
                    <Image
                        source={{uri: "bull"}}
                        style={styles.thumbnail}
                    />
                    <View style={styles.btnRow}>
                        <View style={styles.viewRow}>
                            <Button style={styles.btn}
                                onPress={this._handlePress}
                                title="Play">
                                Play
                            </Button>
                        </View>
                        <View style={styles.viewRow}>
                            <Button style={styles.btn}
                                onPress={this._handlePress}
                                title="Pause">
                                Pause
                            </Button>
                        </View>
                    </View>
                </View>
            );
        }
    }
  });

  var styles = StyleSheet.create({
    navbar: {
      fontWeight: 'bold',
      color: 'white',
      backgroundColor: '#607D8B',
      textAlign: 'center',
      paddingTop: 8,
      paddingBottom: 8,
      fontSize: 16,
    },
    loadingView: {

    },
    view: {
      flex: 1,
      backgroundColor: '#90A4AE'
    },
    inputButton: {
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 15,
      color: '#35c1f4'
    },
    text: {
      textAlign: 'center',
      color: 'white',
      fontSize: 16,
      paddingLeft: 10,
      paddingRight: 10,
      paddingTop: 10
    },
    textSmall: {
        textAlign: 'center',
        color: 'white',
        fontSize: 10, 
        paddingLeft: 10, 
        paddingRight: 10, 
        paddingTop: 10
    },
    heading: {
      fontWeight: 'bold'
    },
    video: {
        //height: 150,
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        borderColor: 'white',
        borderWidth: 0.3, 
        borderStyle: 'dashed',
        marginLeft: 5, 
        marginRight: 5, 
        marginBottom: 5, 
        marginTop: 5
    },
    title: {
      fontSize: 10,
      marginBottom: 8,
      width: 90,
      textAlign: 'center',
    },
    year: {
      textAlign: 'center',
    },
    thumbnail: {
        width: (Dimensions.get('window').width)/3.5,
        height: (Dimensions.get('window').width)/5, 
    },
    listView: {
      paddingTop: 20
    },
    nowPlayingFooter: {
      //flex: 0,
      borderTopWidth: 3,
      borderColor: 'darkgray',
      backgroundColor: 'white',
      alignItems: 'center',
      paddingTop: 15,
      paddingBottom: 15,
      marginRight: 10,
      marginLeft: 10,
      marginBottom: 10

    },
    trackTitle: {
        fontSize: 20,
        marginBottom: 8,
        textAlign: 'center',
    }, 
    viewRow: {
        marginRight: 15, 
        marginLeft: 15
    },
    btnRow: {
        flexDirection: 'row'
    },
    btn: {
        borderWidth: 1, 
        borderColor: '#35c1f4',
    }
});

AppRegistry.registerComponent('ProtoApp', () => ProtoApp);
